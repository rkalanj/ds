# Opis problema Buffonove igle

# zamislimo da imamo plocu sa paralelnim linijama jednako udaljenim medjusobno
# bacamo iglu/stapic na plocu i gledamo hoce li dodirivati ijednu liniju nakon pada
# koristi se za aproksimaciju broja pi


import numpy as np
from mpi4py import MPI
from math import *
from random import *

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

# n = broj bacanja                                                              
n = np.empty(1)
# h = broj pogodaka                                                             
h = np.empty(1)

# definirane duljina igle i razmak izmedju linija
l = 2
t = 2
            
# ukoliko se broj ponavljanja poveca, vidi se da se priblizavamo vrijednosti pi
# sto znaci da se greska smanjuje                                               

for i in range (0, 50000):
    n += 1
    x = uniform(0, 1)
    y = randrange(0, 180, 1)
    # x = udaljenost centra igle od najblize crte, zadani raspon prema wikipediji                               
    # y = kut izmedju igle i crte, zadani raspon prema wikipediji                                             
    if x <= (l/2)*sin(radians(y)):
        h += 1

if rank == 0:
    n_reduced = np.empty(1)
    h_reduced = np.empty(1)
else:
    n_reduced = None
    h_reduced = None

comm.Reduce(n, n_reduced, op=MPI.SUM, root=0)
comm.Reduce(h, h_reduced, op=MPI.SUM, root=0)

if rank == 0:
    n = n_reduced[0] / size
    h = h_reduced[0] / size
    pi = (2*l*n)/(t*h)
    error = abs(pi - np.math.pi)
    print("pi is approximately %f, error is approximately %f" % (pi, error))
    print("n = %f, h = %f" % (n, h))
 
# ispis sa 50000 i 100000 ponavljanja   
    
# stud706@akari:~$ emacs test2.py
# stud706@akari:~$ mpirun -np 5 python3 test2.py
# pi is approximately 3.139698, error is approximately 0.001895
# n = 50000.000000, h = 31850.200000
# stud706@akari:~$ emacs test2.py
# stud706@akari:~$ mpirun -np 5 python3 test2.py
# pi is approximately 3.143310, error is approximately 0.001717
# n = 100000.000000, h = 63627.200000
    
# vidimo da se razlika smanjuje
