# Opis problema Buffonove igle

# zamislimo da imamo plocu sa paralelnim linijama jednako udaljenim medjusobno
# bacamo iglu/stapic na plocu i gledamo hoce li dodirivati ijednu liniju nakon pada
# koristi se za aproksimaciju broja pi


import numpy as np
from mpi4py import MPI
from math import *
from random import *

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

# n = broj bacanja
n = 0
# h = broj pogodaka
h = 0
# definirane duljina igle i razmak izmedju linija
l = 3
t = 2

# ovo je slucaj kada je duljina igle veca od razmaka izmedju crta
# ukoliko se broj ponavljanja poveca, vidi se da se priblizavamo vrijednosti pi
# sto znaci da se greska smanjuje

for i in range (0, 50000):
    n += 1
    x = 0
    y = randrange(0, 180, 1)
    # posto je definirano l=3, u iducoj formuli x < l/2 * sin(y) odmah sam uvrstio 1,5
    # x = udaljenost centra igle od najblize crte
    # y = kut izmedju igle i crte
    if x < 1.5 * sin(radians(y)):
        h += 1

pi_part = np.empty(1)
pi_part[0] = (2*l*n)/(t*h)

if rank == 0:
    pi_reduced = np.empty(1)
else:
    pi_reduced = None

comm.Reduce(pi_part, pi_reduced, op=MPI.SUM, root=0)

if rank == 0:
    pi = pi_reduced[0] / size
    error = abs(pi - np.math.pi)
    print("pi is approximately %f, error is approximately %f" % (pi, error))
    print("n = %f, h = %f" % (n, h))
 
# ispis sa 50000, 100000 i 1000000 ponavljanja   
    
# stud706@akari:~$ mpirun -np 5 python3 test.py
# pi is approximately 3.016579, error is approximately 0.125013
# n = 50000.000000, h = 49722.000000
# stud706@akari:~$ emacs test.py
# stud706@akari:~$ mpirun -np 5 python3 test.py
# pi is approximately 3.016628, error is approximately 0.124965
# n = 100000.000000, h = 99461.000000
# stud706@akari:~$ emacs test.py
# stud706@akari:~$ mpirun -np 5 python3 test.py
# pi is approximately 3.016809, error is approximately 0.124784
# n = 1000000.000000, h = 994427.000000
    
# vidimo da se razlika smanjuje
